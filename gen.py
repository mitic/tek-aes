import sys
import tek.key
import tek.option

##################################################################################

if len(sys.argv)==2 and sys.argv[1].lower()=="list":
	print "Available instrument options:"
	tek.option.print_all()
	sys.exit()

if len(sys.argv)<4:
	print "Usage:\tkeygen <model> <S/N> [instument option1] [instrument option2] ... - generate options key"
	print "\tkeygen list - list available instrument options"
	sys.exit()

k = tek.key.encode(sys.argv[1].upper(), sys.argv[2].upper(), tek.option.make_mask(sys.argv[3:]))
print k