Optional step: use validate.exe to extract options lists from option keys if you have any
Example: validate.exe 9R66P-69MNQ-7EPRD-PHSQ6-W9DDY-B

Use gen.exe to generate keys. Keys are not additive! You need to include all options you already have when generating a key
to keep them active! I.e. if you already have 350MHz DVM EMBD and want to add FLEX - generate for 350MHz DVM EMBD FLEX
Example: gen.exe MDO3012 C123456 2GHz AERO AFG AUDIO AUTOMAX COMP DVM EMBD ENET FLEX LMT MSO PWR SA TRIG USB VID


Available options:
100MHz	100MHz bandwidth
1GHz	1GHz bandwidth
200MHz	200MHz bandwidth
2GHz	2GHz bandwidth
300MHz	300MHz bandwidth
350MHz	350MHz bandwidth
500MHz	500MHz bandwidth
70MHz	70MHz bandwidth
AERO	Aerospace serial bus
AFG	Arbitrary Function Generator
AUDIO	Audio serial bus key
AUTO	Automotive serial bus
AUTOMAX	Full Automotive serial bus
BETA	Beta release
BW0T1	Upgrade bandwidth from 70MHz to 100MHz
BW0T2	Upgrade bandwidth from 70MHz to 200MHz
BW1T10	Upgrade bandwidth from 100MHz to 1GHz
BW1T2	Upgrade bandwidth from 100MHz to 200MHz
BW1T3	Upgrade bandwidth from 100MHz to 350MHz
BW1T5	Upgrade bandwidth from 100MHz to 500MHz
BW2T10	Upgrade bandwidth from 200MHz to 1GHz
BW2T3	Upgrade bandwidth from 200MHz to 350MHz
BW2T5	Upgrade bandwidth from 200MHz to 500MHz
BW3T10	Upgrade bandwidth from 350MHz to 1GHz
BW3T5	Upgrade bandwidth from 350MHz to 500MHz
BW5T10	Upgrade bandwidth from 500MHz to 1GHz
CAL	Calibration bit for manufacturing test
COMP	Computer serial bus
DDU	Distribution Demo Unit
DEMO	Internal demo unit
DVM	Digital Voltmeter
EMBD	Embedded serial bus
ENET	Ethernet serial bus
FLEX	FlexRay serial bus
LMT	Limit/Mask test
MSO	Mixed Signal Oscilloscope
PWR	Power analysis
SA	Spectrum analyzer maximum input frequency
SEC	Security lockout
TRIG	RF triggering
USB	USB serial bus
VID	HD and Custom Video

all options (just add desired bandwidth option): AERO AFG AUDIO AUTOMAX COMP DVM EMBD ENET FLEX LMT MSO PWR SA TRIG USB VID
