## Tek AES series helper routines

### Supported series

- DPO3k
- MDO3k
- MDO4kC

### Reuirements

- [Python 2.4-2.7](https://www.python.org)
- [PyCrypto (for AES)](https://www.dlitz.net/software/pycrypto/)
- or get prebuilt binaries [here](http://www.voidspace.org.uk/python/modules.shtml#pycrypto)

EXEs can be built with PyInstaller (optional step)
