########################################################################
# options masks/names/descriptions, conversion functions
mask_opts = { 0x0000000002000000 : ("AERO", "Aerospace serial bus"),
	0x0000000004000000 : ("AUDIO", "Audio serial bus key"),
	0x0000000008000000 : ("AUTO", "Automotive serial bus"),
	0x0000000010000000 : ("AUTOMAX", "Full Automotive serial bus"),
	0x0000000020000000 : ("COMP", "Computer serial bus"),
	0x0000000040000000 : ("EMBD", "Embedded serial bus"),
	0x0000000080000000 : ("ENET", "Ethernet serial bus"),
	0x0000000100000000 : ("FLEX", "FlexRay serial bus"),
	0x0000000200000000 : ("USB", "USB serial bus"),
	0x0000004000000000 : ("LMT", "Limit/Mask test"),
	0x0000008000000000 : ("PWR", "Power analysis"),
	0x0000080000000000 : ("TRIG", "RF triggering"),
	0x0000040000000000 : ("VID", "HD and Custom Video"),
	0x0000800000000000 : ("CAL", "Calibration bit for manufacturing test"),
	0x0002000000000000 : ("BETA", "Beta release"),
	0x0004000000000000 : ("DEMO", "Internal demo unit"),
	0x0008000000000000 : ("DDU", "Distribution Demo Unit"),
	0x0000000000000001 : ("70MHz", "70MHz bandwidth"),
	0x0000000000000002 : ("100MHz", "100MHz bandwidth"),
	0x0000000000000004 : ("200MHz", "200MHz bandwidth"),
	0x0000000000000008 : ("300MHz", "300MHz bandwidth"),
	0x0000000000000010 : ("350MHz", "350MHz bandwidth"),
	0x0000000000000020 : ("500MHz", "500MHz bandwidth"),
	0x0000000000000040 : ("1GHz", "1GHz bandwidth"),
	0x0000000000000080 : ("2GHz", "2GHz bandwidth"),
	0x0000000000000200 : ("BW0T1", "Upgrade bandwidth from 70MHz to 100MHz"),
	0x0000000000000400 : ("BW0T2", "Upgrade bandwidth from 70MHz to 200MHz"),
	0x0000000000000800 : ("BW1T2", "Upgrade bandwidth from 100MHz to 200MHz"),
	0x0000000000001000 : ("BW1T3", "Upgrade bandwidth from 100MHz to 300MHz"),
	0x0000000000002000 : ("BW1T3", "Upgrade bandwidth from 100MHz to 350MHz"),
	0x0000000000004000 : ("BW1T5", "Upgrade bandwidth from 100MHz to 500MHz"),
	0x0000000000008000 : ("BW2T3", "Upgrade bandwidth from 200MHz to 350MHz"),
	0x0000000000010000 : ("BW2T5", "Upgrade bandwidth from 200MHz to 500MHz"),
	0x0000000000020000 : ("BW3T5", "Upgrade bandwidth from 300MHz to 500MHz"),
	0x0000000000040000 : ("BW3T5", "Upgrade bandwidth from 350MHz to 500MHz"),
	0x0000000000080000 : ("BW1T10", "Upgrade bandwidth from 100MHz to 1GHz"),
	0x0000000000100000 : ("BW2T10", "Upgrade bandwidth from 200MHz to 1GHz"),
	0x0000000000200000 : ("BW3T10", "Upgrade bandwidth from 350MHz to 1GHz"),
	0x0000000000400000 : ("BW5T10", "Upgrade bandwidth from 500MHz to 1GHz"),
	0x0000000400000000 : ("DVM", "Digital Voltmeter"),
	0x0000000800000000 : ("AFG", "Arbitrary Function Generator"),
	0x0000001000000000 : ("MSO", "Mixed Signal Oscilloscope"),
	0x0000002000000000 : ("SA", "Spectrum analyzer maximum input frequency"),
	0x0000000000000100 : ("SEC", "Security lockout") }

name_opts = {	"AERO" : (0x0000000002000000, "Aerospace serial bus"),
	"AUDIO" : (0x0000000004000000, "Audio serial bus key"),
	"AUTO" : (0x0000000008000000, "Automotive serial bus"),
	"AUTOMAX" : (0x0000000010000000, "Full Automotive serial bus"),
	"COMP" : (0x0000000020000000, "Computer serial bus"),
	"EMBD" : (0x0000000040000000, "Embedded serial bus"),
	"ENET" : (0x0000000080000000, "Ethernet serial bus"),
	"FLEX" : (0x0000000100000000, "FlexRay serial bus"),
	"USB" : (0x0000000200000000, "USB serial bus"),
	"LMT" : (0x0000004000000000, "Limit/Mask test"),
	"PWR" : (0x0000008000000000, "Power analysis"),
	"TRIG" : (0x0000080000000000, "RF triggering"),
	"VID" : (0x0000040000000000, "HD and Custom Video"),
	"CAL" : (0x0000800000000000, "Calibration bit for manufacturing test"),
	"BETA" : (0x0002000000000000, "Beta release"),
	"DEMO" : (0x0004000000000000, "Internal demo unit"),
	"DDU" : (0x0008000000000000, "Distribution Demo Unit"),
	"70MHz" : (0x0000000000000001, "70MHz bandwidth"),
	"100MHz" : (0x0000000000000002, "100MHz bandwidth"),
	"200MHz" : (0x0000000000000004, "200MHz bandwidth"),
	"300MHz" : (0x0000000000000008, "300MHz bandwidth"),
	"350MHz" : (0x0000000000000010, "350MHz bandwidth"),
	"500MHz" : (0x0000000000000020, "500MHz bandwidth"),
	"1GHz" : (0x0000000000000040, "1GHz bandwidth"),
	"2GHz" : (0x0000000000000080, "2GHz bandwidth"),
	"BW0T1" : (0x0000000000000200, "Upgrade bandwidth from 70MHz to 100MHz"),
	"BW0T2" : (0x0000000000000400, "Upgrade bandwidth from 70MHz to 200MHz"),
	"BW1T2" : (0x0000000000000800, "Upgrade bandwidth from 100MHz to 200MHz"),
	"BW1T3" : (0x0000000000001000, "Upgrade bandwidth from 100MHz to 300MHz"),
	"BW1T3" : (0x0000000000002000, "Upgrade bandwidth from 100MHz to 350MHz"),
	"BW1T5" : (0x0000000000004000, "Upgrade bandwidth from 100MHz to 500MHz"),
	"BW2T3" : (0x0000000000008000, "Upgrade bandwidth from 200MHz to 350MHz"),
	"BW2T5" : (0x0000000000010000, "Upgrade bandwidth from 200MHz to 500MHz"),
	"BW3T5" : (0x0000000000020000, "Upgrade bandwidth from 300MHz to 500MHz"),
	"BW3T5" : (0x0000000000040000, "Upgrade bandwidth from 350MHz to 500MHz"),
	"BW1T10" : (0x0000000000080000, "Upgrade bandwidth from 100MHz to 1GHz"),
	"BW2T10" : (0x0000000000100000, "Upgrade bandwidth from 200MHz to 1GHz"),
	"BW3T10" : (0x0000000000200000, "Upgrade bandwidth from 350MHz to 1GHz"),
	"BW5T10" : (0x0000000000400000, "Upgrade bandwidth from 500MHz to 1GHz"),
	"DVM" : (0x0000000400000000, "Digital Voltmeter"),
	"AFG" : (0x0000000800000000, "Arbitrary Function Generator"),
	"MSO" : (0x0000001000000000, "Mixed Signal Oscilloscope"),
	"SA" :  (0x0000002000000000, "Spectrum analyzer maximum input frequency"),
	"SEC" : (0x0000000000000100, "Security lockout"), 
	"SA3" : (0x0000100000000000, "Spectrum analyzer 3GHz maximum input frequency"),
	"SA6" : (0x0000200000000000, "Spectrum analyzer 6GHz maximum input frequency"),
	}

def print_all():
	keys = name_opts.keys()
	keys.sort()
	for opt in keys:
		print "%s\t%s" % (opt, name_opts[opt][1])

def print_mask(mask):
	for i in xrange(64):
		m = 1<<i
		if mask & m:
			if m in mask_opts:
				print "%s\t%s" % (mask_opts[m][0], mask_opts[m][1])
			else:
				print "Unknown option %016X" % (m)

def make_mask(names): # input - list of option names to include into mask
	opts = 0
	for name in names:
		if name in name_opts:
			opts |= name_opts[name][0]
		else:
			print "Unknown option: %s" % (name)
	return opts

__all__ = [ "print_all", "print_mask", "make_mask" ]
