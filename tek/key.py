from Crypto.Cipher import AES # from PyCrypto package
from crc16 import crc16
from struct import pack, unpack

########################################################################
# for debug out
def outhex(s):
	if len(s)<=32:
		for b in s:
			print "%02X" % (ord(b)),
	else:
		for b in s[0:16]:
			print "%02X" % (ord(b)),
		print "...",
		for b in s[-16:]:
			print "%02X" % (ord(b)),
	print ""


########################################################################
# Tek's AES keys
mdo3k_key = "\x3D\xD6\x3D\xEF\xAC\x59\x7E\xB4\x3E\xF0\xFD\xE3\x16\xCD\x42\x1F"
dpo3k_key = "\x9B\x31\x62\x2A\x2A\x34\x06\x7F\x64\x07\x1F\x90\xC2\xDE\x6A\x80"			
#mdo4kc_key= "\x5E\x70\xF1\x30\x11\xF3\x3E\xAF\x1B\xBF\x11\x88\xC5\x6F\x22\xB2" # fake
mdo4kb_key = "\x9B\x31\x62\x2A\x2A\x34\x06\x7F\x64\x07\x1F\x90\xC2\xDE\x6A\x80"
mdo4kc_key = "\x47\x4C\xCC\xE6\x80\x29\xD4\x17\xD2\xDC\x81\x5D\xD5\xA8\xAC\x6A"
keys = (("MDO3000", mdo3k_key), ("DPO3000", dpo3k_key), ("MDO4000B", mdo4kb_key), ("MDO4000C", mdo4kc_key))
###################################################################################
# key string bin<->ascii conversion
AsciiToBin = {  'A':0x00, 'B':0x01, 'C':0x02, 'D':0x03, 'E':0x04, 'F':0x05, 'G':0x06, 'H':0x07, 
		'J':0x08, 'K':0x09, 'L':0x0A, 'M':0x0B, 'N':0x0C, 'P':0x0D, 'Q':0x0E, 'R':0x0F,
		'S':0x10, 'T':0x11, 'U':0x12, 'V':0x13, 'W':0x14, 'X':0x15, 'Y':0x16, 'Z':0x17,
		'a':0x00, 'b':0x01, 'c':0x02, 'd':0x03, 'e':0x04, 'f':0x05, 'g':0x06, 'h':0x07, 
		'j':0x08, 'k':0x09, 'l':0x0A, 'm':0x0B, 'n':0x0C, 'p':0x0D, 'q':0x0E, 'r':0x0F,
		's':0x10, 't':0x11, 'u':0x12, 'v':0x13, 'w':0x14, 'x':0x15, 'y':0x16, 'z':0x17,
		'2':0x18, '3':0x19, '4':0x1A, '5':0x1B, '6':0x1C, '7':0x1D, '8':0x1E, '9':0x1F }

BinToAscii = [  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R',
		'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '2', '3', '4', '5', '6', '7', '8', '9' ]

def KeyToBin(key):
	result = ''
	out_pos = 0
	out_val = 0
	for c in key:
		b = AsciiToBin[c]
		out_val |= b<<out_pos
		out_pos += 5
		if out_pos>=8:
			result += chr(out_val & 0xFF)
			out_val >>= 8
			out_pos -= 8
	if out_pos!=0:
		result += chr(out_val & 0xFF)
	return result

def BinToKey(bin):
	out_len = len(bin) * 8 // 5
	if len(bin) * 8 % 5 != 0:
		out_len += 1
	result = ''
	in_val = 0
	in_bits = 0
	in_pos = 0
	out_cnt = 0
	for i in xrange(out_len):
		if in_bits<5:
			if in_pos<len(bin):
				in_val |= ord(bin[in_pos])<<in_bits
			# else will "or with zeroes"
			in_bits += 8
			in_pos += 1
		result+=BinToAscii[in_val & 0x1F]
		out_cnt += 1
		if (out_cnt % 5)==0:
			result += '-'
		in_val>>=5
		in_bits-=5
	return result.strip('-')

#################################################################################
# instrument UID generation
def GenerateUID(model, sn):
	out_ix = 0
	buf = bytearray(6) # also zero-initializes
	for c in model:
		buf[out_ix % 6] ^= ord(c)
		out_ix += 1
	for c in sn:
		buf[out_ix % 6] ^= ord(c)
		out_ix += 1

	m_crc = crc16(model)
	s_crc = crc16(sn)
	buf[0] ^= m_crc>>8	
	buf[3] ^= m_crc>>8
	buf[1] ^= m_crc & 0xFF
	buf[4] ^= m_crc & 0xFF
	buf[1] ^= s_crc>>8	
	buf[4] ^= s_crc>>8	
	buf[2] ^= s_crc & 0xFF
	buf[5] ^= s_crc & 0xFF

	res = ""
	for i in xrange(6):
		res += chr(buf[5-i])		
	return res
	
##################################################################################
# validate an option key, return options mask

def decr(key, ct):
	pt = AES.new(key, AES.MODE_CBC, "\x00"*16).decrypt(ct[0:len(ct)&0xF0])
	#print "PT:",
	#outhex(pt)
	num_data_bits = ord(pt[0])
	if len(pt)<(9+num_data_bits//8):
		#print "Num data bits invalid !"
		return None, None
	#            l, uid    for crc      payload bytes              payload last bits
	pt_for_crc = pt[0:7] + "\x00\x00" + pt[9:9+num_data_bits//8] + chr(ord(pt[9+num_data_bits//8]) & (0xFF>>(8-num_data_bits%8)))
	pt_for_crc += "\x00"*(0x2F-len(pt_for_crc)) # pad to 0x2F len
	#outhex(pt)
	crc = crc16(pt_for_crc)
	#print "CRC: %04X" % (crc)
	if ord(pt[7])!=crc&0xFF or ord(pt[8])!=crc>>8:
		#print "CRC mismatch !"
		return None, None
	return pt[1:7], unpack("<Q", pt_for_crc[9:9+8])[0]

def decode(key, model=None, sn=None):
	b = KeyToBin(key.replace('-', ''))

	uid = None
	mask = None
	for k in keys:
		uid, mask = decr(k[1], b)
		if uid:
			print "This key is for", k[0], "family"
			break
	if not uid:
		print "No decryption key found for this keystring !"
		return None

	if model and sn and uid!=GenerateUID(model, sn):
		print "UID mismatch !"
		return None
	if not model or not sn:
		print "This key is for UID:",
		outhex(uid)
	return mask

##################################################################################
# generate an option key
def encode(model, sn, mask):
	if model.startswith("MDO4") and model.endswith("C"):
		k = mdo4kc_key
	if model.startswith("MDO4") and model.endswith("B"):
		k = mdo4kb_key
	elif model.startswith("MDO"):
		k = mdo3k_key
	else:
		k = dpo3k_key
	uid = GenerateUID(model, sn)
	# find first leading 1 bit
	num_data_bits = 0
	for i in xrange(64):
		if mask & 1<<(63-i):
			num_data_bits = 64-i
			break		
	payload = pack("<Q", (mask))
	pt_for_crc = chr(num_data_bits) + uid + "\x00\x00" + payload + "\x00"*(0x2F-1-6-2-8)
	crc = crc16(pt_for_crc)

	pt_for_enc = chr(num_data_bits) + uid + pack("<H", (crc)) + payload
	key = BinToKey(AES.new(k, AES.MODE_CBC, "\x00"*16).encrypt(pt_for_enc[0:16]))

	return key

##################################################################################

__all__ = [ "decode", "encode" ]
